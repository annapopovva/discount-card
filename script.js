    let name = document.getElementById('name').value;
    let city = document.getElementById('city').value;
    let email = document.getElementById('email').value;
    let category = document.getElementById('category').value;
    let accumulation = document.getElementById('accumulation').value;
    let discount = document.getElementById('discount').value;
    let expDate = document.getElementById('expDate').value;

    let table = document.getElementById("table");
    let rows = table.getElementsByTagName("tr");
    let cells = table.getElementsByTagName("td");
    let listCustomers = document.getElementById('cards-list');
  

/*  CREATE A CARD  */
class Customer { 
    constructor(name, city, email, category, accumulation, discount, code, expDate, customerID)  {
        this.name = name;
        this.city = city; 
        this.email = email; 
        this.category = category; 
        this.accumulation = accumulation;
        this.discount = discount; 
        this.code = code;
        this.expDate = expDate;
        this.customerID = customerID;
    }
}


/*  UI TASKS  */

// Validate the form 
const validateForm = () => {
    if (name.trim() === '' || 
        city.trim() === '' || 
        email.trim() === '' || 
        category.trim() === '' || 
        +category < 1 && 
        +category > 4 || 
        accumulation.trim() === '' || 
        +accumulation < 0 && 
        +accumulation > 1 || 
        discount.trim() === '' || 
        +discount !== 5 &&
        +discount !== 10 &&
        +discount !== 15 &&
        +discount !== 30 
        ) {    
            alert('Please enter valid information! (category between 1 and 4)');
        } 
};


// Create card for each customer 
const renderCustomerCard = customer => {
    const createCard = document.getElementById('card-holder');
    createCard.innerHTML = `
        <div class="card front">
            <div class="grey-front"></div>
            <div class="white-front"></div>
            <div class="red-front"></div>
            <div class="dots"></div>
            <div class="personal-intro">
                <p id="cardName">${customer.name}</p>
                <p>Discount Card</p>
            </div>
        </div>

        <div class="card back">
            <div class="red-back"></div>
            <div class="top dots"></div>
            <div class="personal-info">
                <p id="cardName">${customer.name}</p>
                <p id="cardCity">${customer.city}</p>
                <p id="cardCode">Card Code: ${customer.code}</p>
                <p id="cardEmail">Email: ${customer.email}</p>
                <p id="cardDate">Expiration date: ${customer.expDate}</p>
            </div>
            <div class="bottom dots"></div>
            <div class="grey-back"></div>
        </div>
    `
};


// Add new customer to the list 
const addCustomerToList = (customer) => {
    const row = document.createElement('tr');

    row.innerHTML = `
        <td>${customer.name}</td>
        <td>${customer.city}</td>
        <td>${customer.email}</td>
        <td>${customer.category}</td>
        <td>${customer.accumulation}</td>
        <td>${customer.discount}</td>
        <td>${customer.code}</td>
        <td class="expDate-td">${customer.expDate}</td>
        <td class="delete-row" customerDelId="${customer.customerid}" onclick="deleteRow(this)">X</td>
        `;

    // Adding data to the table
    listCustomers.appendChild(row);

     // Call the edit function on every added customer 
    selectRowToEdit();
};


// Display the student's info in the table
const displayCustomers = () => {
    const customers = getCustomers();

    customers.forEach(customer => addCustomerToList(customer));
};

// Delete the student's info from the table
const deleteRow = r => {
    let i = r.parentNode.rowIndex;
    table.deleteRow(i);      
}; 


// Create a variable to store the index of the selected customer
let rowIndex;

// Display selected row data into form's inputs
const selectRowToEdit = () => {
    
    for (let i = 1; i < table.rows.length; i++) {
        
        rows[i].onclick = () => {
            if(!(rows[i].matches('.delete-row'))) {

                // Change submit to edit button
                document.querySelector('#submit-btn').disabled = true;
                document.querySelector('#edit-btn').style.display = 'block';
                document.querySelector('#submit-btn').style.display = 'none';
    
                // Get the selected row index 
                rowIndex = i; 
    
                // Show table data to the form
                document.getElementById('name').value = rows[i].getElementsByTagName('td')[0].innerHTML;
                document.getElementById('city').value = rows[i].getElementsByTagName('td')[1].innerHTML;
                document.getElementById('email').value = rows[i].getElementsByTagName('td')[2].innerHTML;
                document.getElementById('category').value = rows[i].getElementsByTagName('td')[3].innerHTML;
                document.getElementById('accumulation').value = rows[i].getElementsByTagName('td')[4].innerHTML;
                document.getElementById('discount').value = rows[i].getElementsByTagName('td')[5].innerHTML;
0

                 // Show the selected customer's card 
                 document.getElementById('card-holder').style.display = 'inline-block';
                document.getElementById('cardName').value = rows[i].getElementsByTagName('td')[0].innerHTML;
                document.getElementById('cardCity').value = rows[i].getElementsByTagName('td')[1].innerHTML;
                document.getElementById('cardEmail').value = rows[i].getElementsByTagName('td')[2].innerHTML;
                document.getElementById('cardCode').value = rows[i].getElementsByTagName('td')[6].innerHTML;
                document.getElementById('cardDate').value = rows[i].getElementsByTagName('td')[7].innerHTML;
            }
        };

    }
};

// Save edited customer's values to the table
const updateEditedRow = () => {

    table.rows[rowIndex].getElementsByTagName('td')[0].innerHTML = document.getElementById('name').value;
    table.rows[rowIndex].getElementsByTagName('td')[1].innerHTML = document.getElementById('city').value;
    table.rows[rowIndex].getElementsByTagName('td')[2].innerHTML = document.getElementById('email').value;
    table.rows[rowIndex].getElementsByTagName('td')[3].innerHTML = document.getElementById('category').value;
    table.rows[rowIndex].getElementsByTagName('td')[4].innerHTML = document.getElementById('accumulation').value;
    table.rows[rowIndex].getElementsByTagName('td')[5].innerHTML = document.getElementById('discount').value;
};

 
// Search NAME in the table  
const searchNameInTable = () => {
    let searchedValue = document.getElementById('search-bar').value.toUpperCase();

    for(let i = 0; i < rows.length; i++) {
        let td = rows[i].getElementsByTagName('td')[0];

        if(td) {
            let textValue = td.textContent || td.innerHTML;
            
            if(textValue.toUpperCase().indexOf(searchedValue) > -1) {
                rows[i].style.display = '';
            } else { 
                rows[i].style.display = 'none';
            }
        } 
    }
};

/* // Search CITY in the table  
const searchCityInTable = () => {
    let searchedValue = document.getElementById('search-bar').value.toUpperCase();

    for(let i = 0; i < rows.length; i++) {
        let td = rows[i].getElementsByTagName('td')[1];

        if(td) {
            let textValue = td.textContent || td.innerHTML;
            
            if(textValue.toUpperCase().indexOf(searchedValue) > -1) {
                rows[i].style.display = '';
            } else { 
                rows[i].style.display = 'none';
            }
        } 
    }
};

// Search Name in the table  
const searchCardCodeInTable = () => {
    let searchedValue = document.getElementById('search-bar').value.toUpperCase();

    for(let i = 0; i < rows.length; i++) {
        let td = rows[i].getElementsByTagName('td')[6];

        if(td) {
            let textValue = td.textContent || td.innerHTML;
            
            if(textValue.toUpperCase().indexOf(searchedValue) > -1) {
                rows[i].style.display = '';
            } else { 
                rows[i].style.display = 'none';
            }
        } 
    }
}; */


// Filter data in the table
const filterTable = () => {
    let dropdown = document.getElementById('#accumulation-filter').options[document.querySelector('#accumulation-filter').selectedIndex].value;
  
    for(let i = 0; i < tr.length; i++) {
        let th = rows[i].getElementsByTagName('th')[4];

        if(th) {
            let textValue = th.innerHTML;
            
            if(textValue.indexOf(dropdown) > -1) {
                rows[i].style.display = '';
            } else { 
                rows[i].style.display = 'none';
            }
        } 
    }
  };
 
// Clear the input fields on refresh 
const clearInputs = () => {
    const allInputs = document.querySelectorAll('input');

    for(const input of allInputs) {
        input.value = '';
    }
};


// Set limit for discount cards > 4
const cardLimit = email => {
    const customers = getCustomers();
    
    for (const customer of customers) {
        if(customer.email == email && customer.email.length > 4) {
            document.getElementById('submit-btn').disabled = true;
            document.getElementById('card-limit-alert').classList.add('visible');
            setTimeout(() => document.getElementById('card-limit-alert').remove(), 3000);
            clearInputs();
        }
    }
};



/*  LOCAL STORAGE  */

// Create unique id for each customer 
const customerID = () => {
    let ALPHABET = '0123456789abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZ';

    let ID_LENGTH = 8; 
    
    var rtn = '';
    for (var i = 0; i < ID_LENGTH; i++) {
        rtn += ALPHABET.charAt(Math.floor(Math.random() * ALPHABET.length));
    }
    
    return rtn;
};


// Get the existing data 
const getCustomers = () => {
    let customers;
    if(localStorage.getItem('customers') === null) {
        customers = [];
    } else { 
        customers = JSON.parse(localStorage.getItem('customers'));
    }
    console.log("Success");
    return customers;
};


// Add customer's data to the storage 
const storeCustomer = customer => {
    const customers = getCustomers();

    customers.push(customer);
    localStorage.setItem('customers', JSON.stringify(customers));

};


// Remove student's data from the storage 
const removeStoredCustomer = () => {
    const customers = getCustomers();
    //let customerDelId = document.querySelector(`.delete-row[customerDelId="${customer.customerid}"]`);
    let customerDelId = localStorage.getItem('customerID');
    console.log(customerDelId)

    customers.forEach((customer, index) => {
        if(index == customerDelId) {
          
          console.log(index)
          localStorage.setItem('customers', JSON.stringify(customers));
        }

      });

};
   


/*  EVENTS  */

// DISPLAY customers
document.addEventListener('DOMContentLoaded', displayCustomers);


let customerArray = [];
// ADD new customer 
document.querySelector('.card-form').addEventListener('submit', e => {
    e.preventDefault();

    // Get inputs values
    const name = document.getElementById('name').value;
    const city = document.getElementById('city').value;
    const email = document.getElementById('email').value;
    const category = document.getElementById('category').value;
    const accumulation = document.getElementById('accumulation').value;
    const discount = document.getElementById('discount').value;
    let expDate = document.getElementById('expDate').value;

        let codeDate;
        if (expDate == '') {
            let currentDate = new Date();
            const day = currentDate.getDate();
            const month = currentDate.getMonth() + 1;
            const year = currentDate.getFullYear() + 1;
            expDate = `${day}.${month}.${year}`;
            codeDate = expDate.toString().split('.').join("")
        } else {
            codeDate = expDate.toString().split('-').join("");
        }
    
        const code = `${category}${accumulation}${discount}${codeDate}`;
        const customerid = customerID();
    
        /* if (!cardLimit(email) || !validateForm()) { */

         // Create new customer 
         const customer = new Customer(name, city, email, category, accumulation, discount, code, expDate, customerid);
         customerArray.push(customer);
            
             // Add customer to the list 
             addCustomerToList(customer);
        
             // Render customer's card on the UI 
             renderCustomerCard(customer);
        
             // Add customers's data to local storage
             storeCustomer(customer);
        
             // Clear all inputs
             clearInputs();
        //}
    
});


// SHOW info MESSAGE about the categories
document.querySelector('.info').addEventListener('click', () => {
    document.getElementById('category-alert').classList.add('visible');
});

// HIDE the info MESSAGE about the categories
document.querySelector('.btn-ok').addEventListener('click', () => {
    document.getElementById('category-alert').classList.remove('visible');
});


// SEARCH DATA in the table - name, city and card number

// Search by NAME
document.querySelector('#search-bar').addEventListener('keyup', () => {
    searchNameInTable();
    searchCityInTable();
    searchCardCodeInTable();
});


// FILTER 
document.querySelector('#accumulation-filter').addEventListener('select', filterTable); 


// REMOVE customer from the storage
/* document.querySelector('#cards-list').addEventListener('click', e => {
    if(e.target.matches('.delete-row')) {
        document.getElementById('delete-alert').classList.add('visible');

        document.querySelector('.btn-yes').addEventListener('click', () => {
            document.getElementById('delete-alert').classList.remove('visible');
            removeStoredCustomer();
        });

        document.querySelector('.btn-cancel').addEventListener('click', () => {
            document.getElementById('delete-alert').classList.remove('visible');
        }); 
    }

});  */


// EDIT customer's information and update the table
document.querySelector('.update-info').addEventListener('click', () => {
    updateEditedRow();

    // Change the edit button with submit again
    document.querySelector('#edit-btn').style.display = 'none';
    document.querySelector('#submit-btn').style.display = 'block';
});
